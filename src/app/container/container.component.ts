import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { IfUser } from '../data/if-user';
import { SessionService } from '../services/session.service';
import { ProcessStateService } from '../services/process-state.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;
  user: IfUser = null;
  close() {
    this.sidenav.close();
  }

  constructor(private session: SessionService, private processState: ProcessStateService, private router: Router) { }

  ngOnInit() {
    this.user = this.session.getUser();
    if (this.router.url.endsWith('/app')) {
      this.router.navigate(['/app/dashboard']);
    }
  }

  onDashboard() {
    this.processState.backToDashboard();
    this.sidenav.close();
  }

}
