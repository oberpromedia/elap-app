import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { AuthGuard } from './guards/auth.guard';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { ContainerComponent } from './container/container.component';
import { QuizPageComponent } from './pages/quiz-page/quiz-page.component';
import { QuizSubmitPageComponent } from './pages/quiz-submit-page/quiz-submit-page.component';

const routes: Routes = [
  { path: 'login', component: LoginPageComponent },
  {
    path: 'app', component: ContainerComponent, canActivate: [AuthGuard], children: [
      { path: 'dashboard', component: DashboardPageComponent },
      { path: 'quiz', component: QuizPageComponent },
      { path: 'submit', component: QuizSubmitPageComponent },
      {
        path: '',
        redirectTo: '/app/dashboard',
        pathMatch: 'full'
      },
    ]
  },
  {
    path: '',
    redirectTo: '/app',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
