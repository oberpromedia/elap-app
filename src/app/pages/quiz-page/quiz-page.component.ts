import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuizService } from 'src/app/services/quiz.service';
import { IfQuiz } from 'src/app/data/if-quiz';
import { IfQuestion } from 'src/app/data/if-question';
import { MatRadioChange } from '@angular/material/radio';
import { IfAnswer } from 'src/app/data/if-answer';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { IfHistory } from 'src/app/data/if-history';
import { ProcessStateService } from 'src/app/services/process-state.service';

@Component({
  selector: 'app-quiz-page',
  templateUrl: './quiz-page.component.html',
  styleUrls: ['./quiz-page.component.scss']
})
export class QuizPageComponent implements OnInit {
  quiz: IfQuiz = null;
  loading: boolean = false;
  error: boolean = false;
  currentQuestion: IfQuestion = null;
  currentIndex: number = 0;
  history: IfHistory[] = [];
  selectedAnswered: IfAnswer[] = [];
  caption = "Weiter";
  constructor(private route: ActivatedRoute, private quizService: QuizService, private processState: ProcessStateService) { }

  ngOnInit() {
    this.loading = true;
    this.history = [];
    this.route.queryParams.subscribe(params => {
      if (params && params.id > 0) {
        this.quizService.getQuiz(params.id).then(quiz => {
          this.quiz = quiz;
          this.loading = false;
          this.error = false;
          if (this.quiz.questions && this.quiz.questions.length > 0) {
            this.currentIndex = 0;
            this.renderQuestion(this.quiz.questions[0]);
          } else {
            this.error = true;
          }
        }).catch(err => {
          this.loading = false;
          this.error = true;
        });
      }
    });
  }

  renderQuestion(question: IfQuestion) {
    this.selectedAnswered = [];
    this.currentQuestion = question;
    this.restoreAnswers(question);
    if (this.currentIndex + 1 == this.quiz.questions.length) {
      this.caption = "Absenden";
    } else {
      this.caption = "Weiter";
    }
  }

  onSingleAnswerChanged(event: MatRadioChange) {
    this.selectedAnswered = [event.value];
  }

  onMultiAnsweredChanged(event: MatCheckboxChange, a: IfAnswer) {
    const index = this.selectedAnswered.findIndex(r => r.id === a.id);
    if (event.checked && index < 0) {
      this.selectedAnswered.push(a);
    } else if (!event.checked && index >= 0) {
      this.selectedAnswered.splice(index, 1);
    }
  }

  onNext() {
    if (!this.inHistory(this.currentQuestion.id)) {
      this.history.push({ question: this.currentQuestion, answers: this.selectedAnswered, completed: true });
    }
    if (this.currentIndex + 1 < this.quiz.questions.length) {
      this.currentIndex++;
      this.renderQuestion(this.quiz.questions[this.currentIndex]);
    } else {
      this.onSubmitQuiz();
    }
  }

  onSubmitQuiz() {
    if (this.history.length === this.quiz.questions.length) {
      this.processState.stepTwo(this.quiz, this.history);
    } else {
      // error
    }
  }

  onPrev() {
    // save current to history if not there
    if (this.inHistory(this.currentQuestion.id)) {
      this.updateHistoryAnswers(this.currentQuestion.id, this.selectedAnswered);
    } else {
      this.history.push({ question: this.currentQuestion, answers: this.selectedAnswered, completed: false });
    }
    // restore history
    if (this.history.length > 0 && this.currentIndex - 1 >= 0) {
      this.currentIndex--;
      this.renderQuestion(this.quiz.questions[this.currentIndex]);
    }
  }

  inHistory(id: number): boolean {
    return this.history.findIndex(r => r.question.id === id) >= 0;
  }

  updateHistoryAnswers(id: number, answers: IfAnswer[]) {
    const e = this.history.find(r => r.question.id === id);
    if (e) {
      e.answers = answers;
    }
  }

  restoreAnswers(question: IfQuestion) {
    const e = this.history.find(r => r.question.id === question.id);
    if (e) {
      this.selectedAnswered = e.answers;
    }
  }

  isSelected(id: number): boolean {
    return this.selectedAnswered.findIndex(r => r.id === id) >= 0;
  }
}
