import { Component, OnInit } from '@angular/core';
import { QuizService } from 'src/app/services/quiz.service';
import { IfQuiz } from 'src/app/data/if-quiz';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements OnInit {

  quizzes: IfQuiz[] = [];
  constructor(private quiz: QuizService, private router: Router) { }

  ngOnInit() {
    this.quiz.getAllQuizzes().then(q => {
      this.quizzes = q;
    }).catch(err => { });
  }

  getImageSrc(id: number): string {
    return environment.baseUrl + "/app/images/" + id;
  }

  openQuiz(quiz) {
    this.router.navigate(['/app/quiz'], { queryParams: { id: quiz.id } });
  }
}
