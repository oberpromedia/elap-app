import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router, private session: SessionService) {
    this.form = this.fb.group({
      username: this.fb.control(null, [Validators.required, Validators.minLength(3)]),
      password: this.fb.control(null, [Validators.required, Validators.minLength(6)])
    });
  }

  ngOnInit() {
    if (this.session.getUser()) {
      this.navigateToDashboard();
    }
  }

  navigateToDashboard() {
    this.router.navigate(['/app']);
  }

  onSubmit() {
    if (this.form.valid) {
      this.auth.login(this.form.value.username, this.form.value.password).then(user => {
        this.navigateToDashboard();
      }).catch(err => {
        this.form.controls["password"].setValue(null);
        this.form.setErrors({ password: 'invalid' });
      });
    }
  }

}
