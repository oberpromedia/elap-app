import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizSubmitPageComponent } from './quiz-submit-page.component';

describe('QuizSubmitPageComponent', () => {
  let component: QuizSubmitPageComponent;
  let fixture: ComponentFixture<QuizSubmitPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizSubmitPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizSubmitPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
