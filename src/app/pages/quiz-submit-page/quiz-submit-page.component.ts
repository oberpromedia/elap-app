import { Component, OnInit } from '@angular/core';
import { ProcessStateService } from 'src/app/services/process-state.service';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
  selector: 'app-quiz-submit-page',
  templateUrl: './quiz-submit-page.component.html',
  styleUrls: ['./quiz-submit-page.component.scss']
})
export class QuizSubmitPageComponent implements OnInit {
  loading: boolean = true;
  error: boolean = false;
  punkte: number = 0;
  fragen: string = "0/0";
  constructor(private processState: ProcessStateService, private quizService: QuizService) { }

  ngOnInit() {
    if (this.processState.isStepOneComplete()) {
      // do things
      this.quizService.validateQuiz(this.processState.quiz.id, this.processState.history).then(res => {
        this.loading = false;
        this.error = false;
        if (res.status) {
          this.punkte = res.data.points;
          this.fragen = res.data.questions.filter(r => r.right).length + "/" + res.data.questions.length;
        } else {
          this.error = true;
        }
      }).catch(err => {
        this.loading = false;
        this.error = true;
      });
    } else {
      this.processState.backToDashboard();
    }
  }

  onDashboard() {
    this.processState.backToDashboard();
  }

}
