import { Injectable } from '@angular/core';
import { IfUser } from '../data/if-user';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private user: IfUser = null;
  private STORAGE_NAME: string = 'E-LEARNING-APP-USER';
  constructor() {
    this.loadUserFromStorage();
    // FIXME for audit only
    this.user = JSON.parse('{"id":1,"token":"01461ac8c51f6dd8ab534dc3e21c048739375254bbe907df45c500b572967ed3b0b4ba3766c2a73a7e495f50c115c5822da882e786d440992a425f5c66955e5f","vorname":"Kevin","nachname":"Kirch","username":"kkirch","points":0}');
  }

  saveSession(user: IfUser) {
    this.user = user;
    this.saveUserToStorage(user);
  }

  getUser(): IfUser {
    return this.user;
  }

  getToken(): string {
    if (this.user && this.user.token) {
      return this.user.token;
    }
    return null;
  }

  removeSession() {
    localStorage.setItem(this.STORAGE_NAME, null);
  }


  private loadUserFromStorage() {
    let user: string = localStorage.getItem(this.STORAGE_NAME);
    if (user && user.length > 0) {
      this.user = JSON.parse(user);
    }
  }

  private saveUserToStorage(user: IfUser) {
    localStorage.setItem(this.STORAGE_NAME, JSON.stringify(user));
  }
}
