import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { IfUser } from '../data/if-user';
import { reject, resolve } from 'q';
import { SessionService } from './session.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private connect: ConnectionService, private session: SessionService, private router: Router) { }

  login(username: string, password: string): Promise<IfUser> {
    return new Promise((resolve, reject) => {
      this.connect.postNoAuth("/auth/login", { username: username, password: password }).then((user: IfUser) => {
        this.session.saveSession(user);
        resolve(user);
      }).catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  logout(): Promise<any> {
    this.session.removeSession();
    return new Promise((resolve, reject) => {
      this.connect.post("/auth/logout", {}).then(res => {
        resolve({});
      }).catch(err => {
        console.log(err);
        resolve({});
      });
    });
  }

  get(): Promise<IfUser> {
    this.session.removeSession();
    return new Promise((resolve, reject) => {
      this.connect.get("/auth/get").then(user => {
        resolve(user);
      }).catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  isLoggedIn(): boolean {
    // fetch async if online
    if (this.connect.isOnline() && this.session.getToken() != null) {
      this.get().then(user => {
        this.session.saveSession(user);
      }).catch(err => {
        console.log(err);
        this.session.saveSession(null);
        this.router.navigate(['/login']);
      });
    }
    return this.session.getUser() != null && this.session.getToken() != null;
  }
}
