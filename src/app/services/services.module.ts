import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './auth.service';
import { ConnectionService } from './connection.service';
import { QuizService } from './quiz.service';
import { SessionService } from './session.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [AuthService, ConnectionService, QuizService, SessionService]
})
export class ServicesModule { }
