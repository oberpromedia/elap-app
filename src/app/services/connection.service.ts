import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  private baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient, private session: SessionService) { }

  post(subpath: string, data?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let header = new HttpHeaders().append("Content-Type", "application/json").append("Authorization", "Bearer " + this.session.getToken()).append("Cache-Control", "no-cache");
      this.httpClient.post(this.baseUrl + subpath, data, { headers: header }).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  postNoAuth(subpath: string, data?: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let header = new HttpHeaders().append("Content-Type", "application/json").append("Cache-Control", "no-cache");
      this.httpClient.post(this.baseUrl + subpath, data, { headers: header }).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  get(subpath: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let header = new HttpHeaders().append("Authorization", "Bearer " + this.session.getToken()).append("Cache-Control", "no-cache");
      this.httpClient.get(this.baseUrl + subpath, { headers: header }).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  getNoAuth(subpath: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let header = new HttpHeaders().append("Cache-Control", "no-cache");
      this.httpClient.get(this.baseUrl + subpath, { headers: header }).subscribe(result => {
        resolve(result);
      }, error => {
        reject(error);
      });
    });
  }

  isOnline(): boolean {
    return navigator.onLine;
  }
}
