import { TestBed } from '@angular/core/testing';

import { ProcessStateService } from './process-state.service';

describe('ProcessStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProcessStateService = TestBed.get(ProcessStateService);
    expect(service).toBeTruthy();
  });
});
