import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { IfQuiz } from '../data/if-quiz';
import { IfQuestion } from '../data/if-question';
import { IfAnswer } from '../data/if-answer';
import { IfHistory } from '../data/if-history';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private connect: ConnectionService) { }

  getAllQuizzes(): Promise<IfQuiz[]> {
    return new Promise((resolve, reject) => {
      this.connect.get('/app/get-all-quizzes').then(quizzes => {
        resolve(quizzes);
      }).catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  getQuiz(id: number): Promise<IfQuiz> {
    return new Promise((resolve, reject) => {
      this.connect.get('/app/get-quiz/' + id).then(quiz => {
        resolve(quiz);
      }).catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }

  validateQuiz(id: number, history: IfHistory[]): Promise<{ status: boolean, data: { points: number, questions: { id: number, right: boolean }[] } }> {
    return new Promise((resolve, reject) => {
      const questions = [];
      history.forEach(h => {
        const answers = h.answers.map(a => { return { id: a.id }; });
        questions.push({ id: h.question.id, answers: answers });
      });
      this.connect.post('/app/submit', { id: id, questions: questions }).then(quiz => {
        resolve(quiz);
      }).catch(err => {
        console.log(err);
        reject(err);
      });
    });
  }
}
