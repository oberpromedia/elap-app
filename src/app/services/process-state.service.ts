import { Injectable } from '@angular/core';
import { IfQuestion } from '../data/if-question';
import { IfQuiz } from '../data/if-quiz';
import { IfHistory } from '../data/if-history';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProcessStateService {
  quiz: IfQuiz = null;
  history: IfHistory[] = null;
  constructor(private router: Router) { }

  reset() {
    this.quiz = null;
    this.history = null;
  }

  stepTwo(quiz: IfQuiz, history: IfHistory[]) {
    this.quiz = quiz;
    this.history = history;
    this.router.navigate(['/app/submit']);
  }

  backToDashboard() {
    this.router.navigate(['/app/dashboard']);
  }

  isStepOneComplete(): boolean {
    return this.quiz && this.history && this.history.length > 0;
  }
}
