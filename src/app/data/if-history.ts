import { IfQuestion } from "./if-question";
import { IfAnswer } from "./if-answer";

export interface IfHistory {
    question: IfQuestion;
    answers: IfAnswer[];
    completed: boolean;
}