export interface IfUser {
    id: number;
    vorname: string;
    nachname: string;
    token: string;
    username: string;
    points: number;
}