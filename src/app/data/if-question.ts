import { QuestionType } from "./question-type.enum";
import { IfAnswer } from "./if-answer";

export interface IfQuestion {
    id: number;
    text: string;
    type: QuestionType;
    answers: IfAnswer[];
}