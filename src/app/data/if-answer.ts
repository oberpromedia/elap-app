export interface IfAnswer {
    id: number;
    text: string;
}