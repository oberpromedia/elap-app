import { IfQuestion } from "./if-question";

export interface IfQuiz {
    id: number;
    title: string;
    imageId: number;
    questions?: IfQuestion[];
}