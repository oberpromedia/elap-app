import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { ServicesModule } from './services/services.module';
import { MaterialModule } from './material/material.module';
import { ContainerComponent } from './container/container.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { QuizPageComponent } from './pages/quiz-page/quiz-page.component';
import { SharedModule } from './shared/shared.module';
import { QuizSubmitPageComponent } from './pages/quiz-submit-page/quiz-submit-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    ContainerComponent,
    PageNotFoundComponent,
    DashboardPageComponent,
    QuizPageComponent,
    QuizSubmitPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    ServicesModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
