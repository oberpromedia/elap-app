import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { HintComponent } from './hint/hint.component';
import { HintLoadingComponent } from './hint-loading/hint-loading.component';

@NgModule({
  declarations: [
    HintComponent,
    HintLoadingComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    HintComponent,
    HintLoadingComponent
  ]
})
export class SharedModule { }
