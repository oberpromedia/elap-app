import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HintLoadingComponent } from './hint-loading.component';

describe('HintLoadingComponent', () => {
  let component: HintLoadingComponent;
  let fixture: ComponentFixture<HintLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HintLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HintLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
